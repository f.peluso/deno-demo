you can compile and run this example source with (in order):

1) deno run --allow-write --allow-read --allow-plugin --allow-net --unstable .\contact.ts

2) deno run --allow-write --allow-read --allow-plugin --allow-net --unstable .\db.ts 

3) deno run --allow-write --allow-read --allow-plugin --allow-net --unstable .\server.ts