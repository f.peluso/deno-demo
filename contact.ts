import db from "./db.ts";

const contactsDb = db.collection("contacts");

export interface Contact {
  name: string;
  phone: number;
}

export const createContact = async (
  { request, response }: { request: any; response: any },
) => {
  try {
    const body = await request.body();
    if (!Object.keys(body).length) {
      response.body = { msg: `Request body can not be empty!` };
      response.status = 400;
      //return c.string("Request body can not be empty!", 400);
    }
    const { name, phone } = body.value;

    const insertedContact = await contactsDb.insertOne({
      name,
      phone,
    });

    response.body = {
      msg: `successully added ${name}, ${phone}`,
    };
    response.status = 200;
  } catch (error) {
    response.body = { msg: `Error in insert` };
    response.status = 500;
  }
};
