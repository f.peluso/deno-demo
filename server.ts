import { Application, Router } from "https://deno.land/x/oak/mod.ts";

import * as api from "./contact.ts";

const port = 8000;

const router = new Router();
router.post("/contacts", api.createContact);

const app = new Application();

app.use(router.routes());
app.use(router.allowedMethods());

console.log(`Listening on port ${port}...`);

app.use((ctx) => {
  ctx.response.body = "Hello World!";
});

await app.listen({ port: 8000 });
